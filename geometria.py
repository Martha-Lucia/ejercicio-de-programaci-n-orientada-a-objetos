# Autora = Martha Cango
# Email = martha.cango@unl.edu.ec

from math import sqrt


class Punto:
    x = 0
    y = 0

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return '({},{})'.format(self.x, self.y)

    def cuadrante (self):
        if self.x == 0 and self.y != 0:
            print("{}: Este punto se sitúa sobre el eje Y".format(self))
        elif self.x != 0 and self.y == 0:
            print("{}: Este punto se sitúa sobre el eje X".format(self))
        elif self.x == 0 and self.y == 0:
            print("{}: Este punto se sitúa sobre el origen".format(self))
        elif self.x == 5 and self.y == 5:
            print("{}: El punto B se encuentra lejos del origen".format(self))
        elif self.x > 0 and self.y > 0:
            print("{}: Este punto corresponde al Primer Cuadrante".format(self))
        elif self.x < 0 and self.y > 0:
            print("{}: Este punto corresponde al Segundo Cuadrante".format(self))
        elif self.x < 0 and self.y < 0:
            print("{}: Este punto corresponde al Tercer Cuadrante".format(self))
        else:
            print("{}: Este punto corresponde al Cuarto Cuadrante".format(self))

    def vector(self, p):
        """Calcular el vector resultante entre dos puntos"""
        print("El vector resultante de los puntos {} y {} es: ({}, {})".format(self, p, p.x - self.x, p.y - self.y))

    def distancia(self, p):
        """Calcular la distancia entre dos puntos"""
        distancia = sqrt((p.x - self.x) ** 2 + (p.y - self.y) ** 2)
        print("La distancia que hay entre los puntos {} y {} es: {}".format(self, p, distancia))


class Rectangulo:
    punto_inicial = Punto
    punto_final = Punto

    def __init__(self, punto_inicial, punto_final):
        self.punto_inicial = punto_inicial
        self.punto_final = punto_final
        self.rect_base = (self.punto_final.x - self.punto_inicial.x)
        self.rect_altura = (self.punto_final.y - self.punto_inicial.y)
        self.rect_area = self.rect_base * self.rect_altura

    def base(self):
        print("La base del rectángulo es: {}".format(self.rect_base))

    def altura(self):
        print("La altura del rectángulo es: {}".format(self.rect_altura))

    def area(self):
        print("El área del rectángulo es: %.2f" % self.rect_area)


A = Punto(2, 3)
print("Punto A:", A)
B = Punto(5, 5)
print("Punto B:", B)
C = Punto(-3, -1)
print("Punto C:", C)
D = Punto(0, 0)
print("Punto D:", D)

A.cuadrante()
C.cuadrante()
D.cuadrante()

A.vector(B)
C.vector(A)

A.distancia(B)
B.distancia(A)

B.cuadrante()

rect = Rectangulo(A, B)
rect.base()
rect.altura()
rect.area()
